import './App.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AddContactModal from './AddContactModal/AddContactModal';
import DeleteContactModal from './DeleteContactModal/DeleteContactModal';
import ChangeContactModal from './ChangeContactModal/ChangeContactModall';

function App() {
  const [users, setUsers] = useState([]);
  const [name, setName] = useState(' ');
  const [tel, setTel] = useState(' ');
  const [modalActive, setModalActive] = useState(false);
  const [deleteModalActive, setDeleteModalActive] = useState(false);
  const [changeModalActive, setChangeModalActive] = useState(false);
  const [id, setId] = useState();
  const [nameValue, setNameValue] = useState({ name: "name" });
  const [telValue, setTelValue] = useState("");

  useEffect(() => {
    axios.get("http://localhost:5000/api/users").then((resp) => {
      setUsers(resp.data);
    });
  }, [setUsers]);

  const getUserName = (id) => {
    let userName = '';
    users.forEach((item, index) => {
      if (index == id) {
        userName = item.name
      }
    });
    return userName
  }

  const getUserTel = (id) => {
    let userTel = '';
    users.forEach((item, index) => {
      if (index == id) {
        userTel = item.tel
      }
    });
    return userTel
  }

  const renderUser = (user, i) => {
    return (
      <div key={i} className="user-item user d-flex">
        <p className="user-name">{user.name}</p>
        <p className="user-tel">{user.tel}</p>
        <button
          className="s-button del-button"
          id={i}
          onClick={(event) => {
            setId(event.target.id);
            setDeleteModalActive(true);
          }}
        >
          Удалить
        </button>
        <button
          className="s-button"
          id={i}
          onClick={(event) => {
            setId(event.target.id);
            setNameValue({ name: getUserName(event.target.id) })
            setTelValue(getUserTel(event.target.id))
            setChangeModalActive(true);
          }}
        >
          Редактировать
        </button>
      </div>
    );
  };

  return (
    <div className="App">
      <div class="limit">
        <div className="list">
          {(users && users.length > 0) ? (
            users.map((user, i) => renderUser(user, i))
          ) : (
            <p>No users found</p>
          )}
        </div>

        <button className="open-create-modal s-button" onClick={() => { setModalActive(true); setName(""); setTel("") }}>Добавить контакт</button>
      </div>

      <AddContactModal
        modalActive={modalActive} setModalActive={setModalActive}
        name={name} setName={setName}
        tel={tel} setTel={setTel}
        users={users} setUsers={setUsers}
      />

      <DeleteContactModal
        deleteModalActive={deleteModalActive} setDeleteModalActive={setDeleteModalActive}
        users={users} setUsers={setUsers}
        id={id}
      />

      <ChangeContactModal
        changeModalActive={changeModalActive} setChangeModalActive={setChangeModalActive}
        users={users} setUsers={setUsers}
        nameValue={nameValue} setNameValue={setNameValue}
        telValue={telValue} setTelValue={setTelValue}
        id={id}
      />

    </div>
  );
}

export default App;