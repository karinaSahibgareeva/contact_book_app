import React, { useEffect, useState } from 'react';
import "../App.css"
import axios from 'axios';

const ChangeContactModal = ({ changeModalActive, setChangeModalActive, nameValue, setNameValue, id, telValue, setTelValue, users, setUsers }) => {
    const [nameDirty, setNameDirty] = useState(false);
    const [telDirty, setTelDirty] = useState(false);
    const [nameError, setNameError] = useState("");
    const [telError, setTelError] = useState("");
    const [formValid, setFormValid] = useState(false);

    const changePost = event => {
        event.preventDefault();
        let nameChange = nameValue.name
        let telChange=telValue
        axios.put("http://localhost:5000/api/user", { id, nameChange, telChange }).then((resp) => {
            setUsers(resp.data);
        });
    }
    useEffect(() => {
        if (nameError || telError) {
            setFormValid(false)
        }
        else{
            setFormValid(true)
        }
    }, [nameError, telError])

    const blurHandler = (e) => {
        switch (e.target.name) {
            case "name":
                setNameDirty(true)
                break

            case "tel":
                setTelDirty(true)
                break
        }
    }

    const nameHandler = event => {
        setNameValue({name:event.target.value})
        if (!event.target.value.trim().length > 0) {
            setNameError("Поле имя не должно быть пустым")
        }
        else {
            setNameError("")
        }
    }

    const telHandler = event => {
        setTelValue(event.target.value)
        if ((event.target.value.trim().length > 11) || (event.target.value.trim().length < 11) || (!event.target.value.trim().length > 0)) {
            setTelError("Поле некорректно")
        }
        else {
            setTelError("")
        }
    }
    return (
        <div className={changeModalActive ? "modal active" : "modal"} onClick={() => {setChangeModalActive(false); setNameError(""); setTelError("")}}>
            <div className="modal-create" onClick={(e) => e.stopPropagation()}>
                <form method="POST" className="modal-form d-flex">
                    <label for="name">Имя</label>
                    {(nameDirty && nameError) && <div style={{ color: "red" }}>{nameError}</div>}
                    <input
                        type="text"
                        name="name"
                        onBlur={e => blurHandler(e)}
                        value={nameValue.name}
                        onChange={(event) =>{
                            nameHandler(event)
                            // setNameValue({name:event.target.value})
                        }}
                        className="modal-input"
                    />
                    <label for="tel">Номер телефона</label>
                    {(telDirty && telError) && <div style={{ color: "red" }}>{telError}</div>}
                    <input
                        type="tel"
                        name="tel"
                        onBlur={e => blurHandler(e)}
                        value={telValue}
                        onChange={(event) => {
                            telHandler(event)
                            // setTelValue(event.target.value)
                        }}
                        className="modal-input"
                    />
                    <div className="modal-btns d-flex">
                        <button
                            type="submit"
                            onClick={(e) => {
                                e.preventDefault(); 
                                setChangeModalActive(false);
                                setNameError(""); setTelError("")
                            }}
                            className="s-button modal-btn"
                        >
                            Отмена
                        </button>
                        <button
                            type="submit"
                            disabled={!formValid}
                            onClick={(e) => {
                                changePost(e);
                                setChangeModalActive(false);
                                setNameError(""); setTelError("")
                            }}
                            className="s-button modal-btn"
                        >
                            Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default ChangeContactModal;