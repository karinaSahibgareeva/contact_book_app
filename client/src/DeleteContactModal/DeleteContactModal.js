import React from 'react';
import "../App.css"
import axios from 'axios';

const DeleteContactModal = ({ deleteModalActive, setDeleteModalActive, users, setUsers, id }) => {
    const deletePost = event => {
        event.preventDefault();
        axios.delete(`http://localhost:5000/api/user/${id}`).then((resp) => {
            setUsers(resp.data);
        })
    }
    const getUserName = () => {
        let userName = 'name';
        users.forEach(function (item, index) {
            if (index == id) {
                userName = item
            }
        });
        return userName.name
    }
    return (
        <div className={deleteModalActive ? "modal active" : "modal"} onClick={() => setDeleteModalActive(false)}>
            <div className="modal-create" onClick={(e) => e.stopPropagation()}>
                <p>Удалить контакт {getUserName()} ?</p>
                <form className="modal-form d-flex">
                    <div className="modal-btns d-flex">
                        <button
                            type="submit"
                            onClick={(e) => { e.preventDefault(); setDeleteModalActive(false) }}
                            className="s-button modal-btn"
                        >
                            Отмена
                        </button>
                        <button
                            type="submit"
                            onClick={(e) => { deletePost(e); setDeleteModalActive(false) }}
                            className="s-button modal-btn"
                        >
                            Удалить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default DeleteContactModal;