import React, { useEffect, useState } from 'react';
import "../App.css"
import axios from 'axios';

const AddContactModal = ({ modalActive, setModalActive, name, setName, tel, setTel, users, setUsers}) => {
    const [nameDirty, setNameDirty] = useState(false);
    const [telDirty, setTelDirty] = useState(false);
    const [nameError, setNameError] = useState("Поле имя не должно быть пустым");
    const [telError, setTelError] = useState("Поле телефон не должно быть пустым");
    const [formValid, setFormValid] = useState(false);

    const createPost = event => {
        event.preventDefault();
        let id = users.length;
        axios.post("http://localhost:5000/api/user", { id, name, tel }).then((resp) => {
            setUsers(resp.data);
        });
    }
    
    useEffect(() => {
        if (nameError || telError) {
            setFormValid(false)
        }
        else{
            setFormValid(true)
        }
    }, [nameError, telError])

    const blurHandler = (e) => {
        switch (e.target.name) {
            case "name":
                setNameDirty(true)
                break

            case "tel":
                setTelDirty(true)
                break
        }
    }

    const nameHandler = event => {
        setName(event.target.value)
        if (!event.target.value.trim().length > 0) {
            setNameError("Поле имя не должно быть пустым")
        }
        else {
            setNameError("")
        }
    }

    const telHandler = event => {
        setTel(event.target.value)
        if ((event.target.value.trim().length > 11) || (event.target.value.trim().length < 11) || (!event.target.value.trim().length > 0)) {
            setTelError("Поле некорректно")
        }
        else {
            setTelError("")
        }
    }

    return (
        <div className={modalActive ? "modal active" : "modal"} onClick={() => { setModalActive(false);
         setNameError("Поле имя не должно быть пустым"); setTelError("Поле телефон не должно быть пустым"); setNameDirty(false); setTelDirty(false); setFormValid(false)
          }}>
            <div className="modal-create" onClick={(e) => e.stopPropagation()}>
                <form method="POST" className="modal-form d-flex">
                    <label for="name">Имя</label>
                    {(nameDirty && nameError) && <div style={{ color: "red" }}>{nameError}</div>}
                    <input
                        type="text"
                        name="name"
                        onBlur={e => blurHandler(e)}
                        value={name}
                        onChange={(event) => 
                            nameHandler(event)
                        }
                        className="modal-input"
                    />
                    <label for="tel">Номер телефона</label>
                    {(telDirty && telError) && <div style={{ color: "red" }}>{telError}</div>}
                    <input
                        type="tel"
                        name="tel"
                        onBlur={e => blurHandler(e)}
                        value={tel}
                        onChange={(event) =>
                            telHandler(event)
                        }
                        className="modal-input"
                    />
                    <div className="modal-btns d-flex">
                        <button
                            type="submit"
                            onClick={(e) => { e.preventDefault(); setModalActive(false); 
                                setNameError("Поле имя не должно быть пустым"); setTelError("Поле телефон не должно быть пустым"); setNameDirty(false); setTelDirty(false); setFormValid(false)
                             }}
                            className="s-button modal-btn"
                        >
                            Отмена
                        </button>
                        <button
                            // className={(name === 'Имя не должно быть пустым') ? "modal-btn save-button" : "modal-btn save-button active"}
                            className="s-button modal-btn"
                            type="submit"
                            disabled={!formValid}
                            onClick={(e) => { createPost(e); setModalActive(false); 
                                setNameError("Поле имя не должно быть пустым"); setTelError("Поле телефон не должно быть пустым"); setNameDirty(false); setTelDirty(false); setFormValid(false)
                            }}
                        >
                            Добавить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default AddContactModal;