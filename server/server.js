import express from "express";
import cors from "cors";
import { Low, JSONFile } from 'lowdb';
import { join } from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const file = join(__dirname, 'db.json')
const adapter = new JSONFile(file)
const db = new Low(adapter)

db.read()
db.data = { users: [{ id: "0", name: "person1", tel: "89371450511" }, { id: "1", name: "person2", tel: "89273250511" }, { id: "2", name: "person3", tel: "89373250533" },  { id: "4", name: "person4", tel: "89275250516" },  { id: "5", name: "person5", tel: "89372154628" }] }
// db.write()
const app = express();
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())

app.get("/api/users", (req, res) => {
  return res.json(db.data.users)
})

app.post("/api/user", (req, res) => {
  const name = req.body.name
  const tel = req.body.tel
  const id = req.body.id

  db.data.users.push({ id: id, name: name, tel: tel })
  db.write()
  return res.json(db.data.users)
})

app.put("/api/user", (req, res) => {
  const name = req.body.nameChange
  const tel = req.body.telChange
  const id = req.body.id

  let dataUsers = db.data.users
  let user = { id: id, name: name, tel: tel };
  dataUsers.splice(id, 1, user);
  db.write();

  return res.json(
    db.data.users
  );
});

app.delete("/api/user/:id", (req, res) => {
  const id = parseInt(req.params.id)

  db.data.users.splice(id, 1);
  db.write()
  res.json(db.data.users)
})

app.listen(5000, () => {
  console.log("Server started on port 5000")
})
